 /* eslint-disable semi */
/* eslint-disable quotes */
/* eslint-disable indent */
/**
 * Copyright 2020 Quantiphi Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

"use strict";

const webhookFullfillment = require("dialogflow-fulfillment-builder");
const config = require("./../config")();
const intentMapper = require("./intent-mapper");
const actionMapper = require("./action-mapper");
const storeConversation = require("../helper/storeSession")
const { getAllFulfillmentTextMessages} = require('../helper/utils');


const getResponse = (fulfillment) => {

    if (fulfillment._response.fulfillmentMessages.length === 0) {
        fulfillment._response.fulfillmentMessages = fulfillment._request.queryResult.fulfillmentMessages;
    }
    if (fulfillment._response.fulfillmentText === '') {
        const fulfillmentText = getAllFulfillmentTextMessages(fulfillment._response.fulfillmentMessages);
        fulfillment._response.fulfillmentText = fulfillmentText;
    }
    return fulfillment;
}



/**
 * Dialogflow fullfillment controller
 * @param {object} req http request 
 * @param {object} res http response
 * @param {function} next invokes the succeeding middleware/function
 */

module.exports = (db) => {
    return async (req, res, next) => {
        try {
            const requestIntent = req.body.queryResult.intent.displayName;
            console.log("intent name:",requestIntent);

            let fulfillment = new webhookFullfillment(config.fullfillmentConfig, req.body);
            const session = fulfillment._request.session.split('/');
            const sessionId = session[session.length - 1];
            const intent = await intentMapper(requestIntent);
            const action = await actionMapper(req.body.queryResult.action);
            try {
                if (intent) {
                    await intent(fulfillment, db);
                }
                else if(req.body.queryResult.action){
                     await action(fulfillment, db);
                } 
            }
            catch (error) {
                // set the error message in the DF response in case of any failure
                   console.log("error",error);    
            }
            fulfillment = getResponse(fulfillment)
            let result = fulfillment.getCompiledResponse();   
            storeConversation.storeConversation(fulfillment,sessionId, requestIntent);
            res.status(200).json(result);
        }
        catch (err) {
            next(err);
        }
    };
};


const getIntent = (name) => {
    let file = name.toLowerCase();
    file = file.replace(/ +/g, "-");
    return `./intents/${file}`;
};