/**
 * Copyright 2020 Quantiphi, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 "use strict"; 
 const { getShortClaim, getLongClaim, sendMessage, updateResponseText } = require("./../../services/api-service");
 const { getShortClaimDetails, getPaymentData } = require("./../../helper/getClaim-data");
 const { getTransaction } = require("./../../helper/transaction");

 /**
  * Default Welcome Intent controller
  * @param {object} df webhook fulfillment object
  */
 
 const paymentTransaction = async (df) =>{

    // get responses from payload
    const responses = df._request.queryResult.fulfillmentMessages[1].payload;

    // get transaction number from DF
    const globalParams = df.getContext("global")["parameters"];
    const transactionNumber = globalParams && parseInt(globalParams["transaction-number"]) || parseInt(0);

    // get phonenumber and claimnumber from CISCO/DF
    const userPhonenumber = globalParams && globalParams["user-phonenumber"] || "";
    const userClaimnumber = globalParams && globalParams["user-claimnumber"] || "";

    // get response from short claim
    const shortClaim = await getShortClaim(userClaimnumber);
    const shortClaimResponse = await getShortClaimDetails(shortClaim, userPhonenumber);

    // get long claim - trnsactions
    const longClaim = await getLongClaim(userClaimnumber, shortClaimResponse.roleID);
    const paymentDetails = await getPaymentData(longClaim)
    const lengthPaymentDetails = paymentDetails && paymentDetails.length || 0;

    // get index
    const index = globalParams && parseInt(globalParams["index"]) || parseInt(1);

    const numTransactions = Math.min(lengthPaymentDetails, 5)

    // set response
    if ((transactionNumber + index) == -1) { 
        const dfResponse = await getTransaction(transactionNumber + index + 1, numTransactions, paymentDetails, responses);
        df.setResponseText(dfResponse);
    
        // update the transaction number variable
        df.setOutputContext("global", 100, 
            {
                "transaction-number": transactionNumber + index + 1
            });
    }
    else if ((transactionNumber + index) == numTransactions) {  
        const dfResponse = await getTransaction(transactionNumber + index - 1, numTransactions, paymentDetails, responses);
        df.setResponseText(dfResponse);
    
        // update the transaction number variable
        df.setOutputContext("global", 100, 
            {
                "transaction-number": transactionNumber + index - 1
            });
    }
    else {
        const dfResponse = await getTransaction(transactionNumber + index, numTransactions, paymentDetails, responses);
        df.setResponseText(dfResponse);
    
        // update the transaction number variable
        df.setOutputContext("global", 100, 
            {
                "transaction-number": transactionNumber + index
            });
    }

    // if last transaction, then update the context
    if ((transactionNumber + index) == numTransactions - 1) {
        df.setOutputContext("success-followup", 1);
    }
};
 
 module.exports = paymentTransaction;
 