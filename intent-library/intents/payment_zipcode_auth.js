/**
 * Copyright 2020 Quantiphi, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 "use strict";

 const { getShortClaim, getLongClaim, sendMessage, updateResponseText } = require("./../../services/api-service");
 const { getShortClaimDetails, getPaymentData } = require("./../../helper/getClaim-data");
 
 /**
  * Default Welcome Intent controller
  * @param {object} df webhook fulfillment object
  */
 
 const paymentZipcodeAuth = async (df) =>{

    // get responses from payload
    const responses = df._request.queryResult.fulfillmentMessages[1].payload;

    // get phonenumber and claimnumber from CISCO/DF
    const globalParams = df.getContext("global")["parameters"];
    const userPhonenumber = globalParams && globalParams["user-phonenumber"] || "";
    const userClaimnumber = globalParams && globalParams["user-claimnumber"] || "";

    // get response from short claim
    const shortClaim = await getShortClaim(userClaimnumber);
    const shortClaimResponse = await getShortClaimDetails(shortClaim, userPhonenumber);

    // zipcode value
    const dbZipcode = shortClaimResponse && shortClaimResponse.zipCode || "";

    // get zipcode mentioned by the user
    const params = df.getContext("paymentzipcodeconfirm-followup")["parameters"];
    const zipcode = params && params["zip-code"] || "";

    // if authenticated
    if (zipcode === dbZipcode) {
        df.setEvent("transaction");
        df.setOutputContext("payment-transaction", 1);
        df.setOutputContext("global", 100,  
            {
                "transaction-number": -1
            });
    }
    // not authenticated
    else {
        // get transaction number from DF
        const attempt = globalParams && globalParams["attempt"] || 0;

        if (attempt == 1) {
            df.setResponseText(responses.transfer_cr);
            df.setOutputContext("global_flow-followup", 1);
            df.clearContext("connect-cr");
        }
        else {
            df.setOutputContext("global", 100, 
                {
                    "attempt": 1
                });
            df.setResponseText(responses.auth_failed);
            df.setOutputContext("zipcode", 1);
            df.setOutputContext("dont-know-zipcode", 1);
        }
    }
};
 
 module.exports = paymentZipcodeAuth;
 