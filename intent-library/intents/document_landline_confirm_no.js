/**
 * Copyright 2020 Quantiphi, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 "use strict";

 /**
  * Default Welcome Intent controller
  * @param {object} df webhook fulfillment object
  */
 
 const documentLandlineConfirmNo = async (df) =>{
     // get responses from payload
    const responses = df._request.queryResult.fulfillmentMessages[1].payload;

    // get transaction number from DF
    const globalParams = df.getContext("global")["parameters"];
    const attempt = globalParams && globalParams["attempt"] || 0;

    if (attempt < 2) {
        df.setOutputContext("global", 100, 
            {
                "attempt": attempt + 1
            });
    }
    else if (attempt >= 2){
        df.setResponseText(responses.limit);
        df.setOutputContext("global_flow_followup", 1);
        df.clearContext("landline-confirm");
        df.clearContext("connect-cr");
    }
    else {
        df.setOutputContext("global", 100, 
            {
                "attempt": 1
            });
    }
};
 
 module.exports = documentLandlineConfirmNo;
 