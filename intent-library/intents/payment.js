/**
 * Copyright 2020 Quantiphi, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 "use strict";

 const { getShortClaim, getLongClaim, sendMessage, updateResponseText } = require("./../../services/api-service");
 const { getShortClaimDetails, getPaymentData } = require("./../../helper/getClaim-data");
 
 /**
  * Default Welcome Intent controller
  * @param {object} df webhook fulfillment object
  */
 
 const payment = async (df) =>{

    // get responses from payload
    const responses = df._request.queryResult.fulfillmentMessages[1].payload;

    // get phonenumber and claimnumber from CISCO/DF
    const globalParams = df.getContext("global")["parameters"];
    const userPhonenumber = globalParams && globalParams["user-phonenumber"] || "";
    const userClaimnumber = globalParams && globalParams["user-claimnumber"] || "";

    // get response from short claim
    const shortClaim = await getShortClaim(userClaimnumber);
    const shortClaimResponse = await getShortClaimDetails(shortClaim, userPhonenumber);

    // zipcode and or birthdate value
    const dbZipcode = shortClaimResponse && shortClaimResponse.zipCode || "";
    const dbBirthdate = shortClaimResponse && shortClaimResponse.dob || "";
    // get long claim - trnsactions
    const longClaim = await getLongClaim(userClaimnumber, shortClaimResponse.roleID);
    const paymentDetails = await getPaymentData(longClaim)
    const lengthPaymentDetails = paymentDetails && paymentDetails.length || 0;
    
    // Calling number available?
    if (userPhonenumber) {
        // Does the phone number match with claim details?
        if (shortClaimResponse && !shortClaimResponse.isMultipleContact) {
            // Check if transaction list available
            if (lengthPaymentDetails) {
                // Check whether zipcode or birthdate is mentioned on the claim. 
                // Zipcode having the higher preference
                // ask for zipcode
                if (dbZipcode) {
                    df.setResponseText(responses.has_zipcode);
                    df.setOutputContext("zipcode", 1);
                    df.setOutputContext("dont-know-zipcode", 1);
                }
                // if zipcode not available then as for birthdate
                else if (dbBirthdate){
                    df.setResponseText(responses.has_birthdate);
                    df.setOutputContext("birthdate", 1); 
                    df.setOutputContext("dont-know-birthdate", 1);               
                }
                // if both are not available
                else {
                    df.setResponseText(responses.no_info);
                    df.setOutputContext("global_flow-followup", 1);
                }
            }
            // no transactions available
            else {
                df.setResponseText(responses.no_transaction);
                df.setOutputContext("global_flow-followup", 1);
            }
        }
        // claim number does not match with the user's phone number
        else {
            df.setResponseText(responses.claim_unmatch);
            df.setOutputContext("global_flow-followup", 1);
        }
    }
    // calling number is not available
    else {
        df.setResponseText(responses.no_claim);
        df.setOutputContext("global_flow-followup", 1);
    }
 };
 
 module.exports = payment;
 