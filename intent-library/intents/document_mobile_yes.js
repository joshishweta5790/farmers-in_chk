/**
 * Copyright 2020 Quantiphi, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 "use strict";

 const { getShortClaim, getLongClaim, sendMessage, updateResponseText } = require("./../../services/api-service");

 /**
  * Default Welcome Intent controller
  * @param {object} df webhook fulfillment object
  */
 
 const documentMobileYes = async (df) =>{

    // get responses from payload
    const responses = df._request.queryResult.fulfillmentMessages[1].payload;

    // get mobile number from DF
    const globalParams = df.getContext("global")["parameters"];
    const mobileNumber = (globalParams && globalParams["user-phonenumber"]) || (params && globalParams["mobile-number"])

    // set regular expression for mobile number
    const mobileNumberRegex = new RegExp(/(^\d{10}$)/);

    // if valid mobile number
    if (mobileNumber && mobileNumber.match(mobileNumberRegex)) {
        // trigger API
        const apiResponse = await sendMessage(mobileNumber, "message", "ID")
        // Mulesoft API call successful
        if (apiResponse) {
            df.setResponseText(responses.api_success);
            df.setOutputContext("payment", 1);
            df.setOutputContext("document", 1);
            df.setOutputContext("other", 1);
            df.setOutputContext("success-followup", 1);
            df.setOutputContext("connect-cr", 1);
        }
        // Mulesoft API call failure
        else {
            df.setResponseText(responses.api_fail);
            df.setOutputContext("global_flow-followup", 1);
        }    
    } 
    // invalid mobile number
    else {
        df.setResponseText(responses.invalid_number);
        df.setOutputContext("landline-confirm", 1);
    }
};
 
 module.exports = documentMobileYes;
 