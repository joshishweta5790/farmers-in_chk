/**
 * Copyright 2020 Quantiphi, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 "use strict";

const { getShortClaim, getLongClaim, sendMessage, updateResponseText } = require("./../../services/api-service");
const { getShortClaimDetails, getPaymentData } = require("./../../helper/getClaim-data");

 /**
  * Default Welcome Intent controller
  * @param {object} df webhook fulfillment object
  */
 
 const paymentDontKnow = async (df) =>{

    // get responses from payload
    const responses = df._request.queryResult.fulfillmentMessages[1].payload;
    
    // check if the claim has zipcode and/or birthdate
    const globalParams = df.getContext("global")["parameters"];
    const userPhonenumber = globalParams && globalParams["user-phonenumber"] || "";
    const userClaimnumber = globalParams && globalParams["user-claimnumber"] || "";

    // get response from short claim
    const shortClaim = await getShortClaim(userClaimnumber);
    const shortClaimResponse = await getShortClaimDetails(shortClaim, userPhonenumber);

    // birthdate value
    const dbBirthdate = shortClaimResponse && shortClaimResponse.dob || "";

    // get params from DF
    const params = df.getContext("global")["parameters"];
    const dontKnowZipcode = params["dont_know_zipcode"];
    const dontKnowBirthdate = params["dont_know_birthdate"];

    // dontKnowBirthdate intent is not yet visited
    if (dontKnowBirthdate == 0) {
        // user dont know zipcode
        if (dontKnowZipcode) { 
            // check if claim has birthdate   
            if (dbBirthdate) {
                // ask for birthdate
                df.setResponseText(responses.ask_birthdate);
                df.setOutputContext("birthdate", 1);
                df.setOutputContext("dont-know-birthdate", 1);
            }
            // authentication failed
            else {
                df.setResponseText(responses.auth_failed);
                df.setOutputContext("global_flow-followup", 1);
            }
        }
    }
    // dont_know_birthdate visited i.e. dont_know_zipcode is visited as well
    // authentication failed
    else {
        df.setResponseText(responses.auth_failed);
        df.setOutputContext("global_flow-followup", 1);
    }

};
 
 module.exports = paymentDontKnow;
 