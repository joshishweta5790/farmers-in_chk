'use strict'

let short_claim = {
   "searchCriteriaResponse": {
      "claims": [{
         "claimNumber": "7000531479",
         "claimAlternateReference": "I4091110",
         "claimSource": "CC",
         "catastropheClaimIndicator": "false",
         "claimSystemName": "GWCC",
         "lossInfo": {
            "lossDateTime": "2019-05-22T00:00:00Z",
            "losspostalAddress": { "state": "TX" }
         },
         "policyInfo": [{
            "policyNumber": "0078477969",
            "policyType": "Trailer",
            "policyTypeCode": "TravelTrailer_Ext"
         }],
         "contact": [{
            "personName": {
               "firstName": "TAISHA",
               "lastName": "WILLIAMS"
            },
            "postalAddress": {
               "addressLines": "10114 ALGIERS RD  ",
               "city": "HOUSTON",
               "state": "TX",
               "zipCode": "77041"
            }
         }],
         "claimContacts": [{
            "personName": {
               "firstName": "TAISHA",
               "lastName": "WILLIAMS"
            },
            "roleId": "ccp:17548988",
            "telephones": [{ "telephoneNumber": "5555555555" }],
            "emailAdresses": [{}],
            "chequePayment": {}
         }],
         "claimAdjusters": [{
            "firstName": "Tow",
            "lastName": "Vendor",
            "fullName": "Tow Vendor",
            "COMIndicator": "Y",
            "telephones": [{ "telephoneNumber": "8002575724" }]
         }],
         "claimExposureOrUnit": [{
            "claimType": "Towing and Road Service",
            "exposureContacts": [{
               "personName": { "fullName": "Tow Vendor" },
               "telephones": [{ "telephoneNumber": "8002575724" }],
               "assignmentStatus": "true",
               "adjusterGroup": "Farmers Tow Vendor Assignment Group"
            }]
         }],
         "associatedPersons": [{
            "personName": {
               "firstName": "TAISHA",
               "lastName": "WILLIAMS"
            },
            "PostalAddresses": [{ "state": "TX" }]
         }]
      }]
   }
}

let long_claim = {
   "Claims": {
      "Policy": {
         "policytype": "Auto",
         "coverageSource": "APPSCE",
         "propertyAddress": {
            "postalCode": "871212395",
            "country": "United States"
         },
         "VehicleList": [{
            "vehicle": {
               "Coveragelist": [
                  {
                     "coverage": {
                        "Type": "Bodily Injury",
                        "UniqueCoverageId": "ccsit:45930563",
                        "CoverageCode": "13023",
                        "Deductible": "0.00 usd"
                     }
                  },
                  {
                     "coverage": {
                        "Type": "Property Damage",
                        "UniqueCoverageId": "ccsit:45930564",
                        "CoverageCode": "13024",
                        "Deductible": "0.00 usd",
                        "CovTermList": [
                           {
                              "covTerm": {
                                 "covTermPattern": "Property Damage (Vehicle and Property)",
                                 "coverageCategory": "Limit",
                                 "note": "Vehicle Damage"
                              }
                           },
                           {
                              "covTerm": {
                                 "covTermPattern": "Property Damage (Vehicle and Property)",
                                 "amount": "0.00",
                                 "coverageCategory": "Limit",
                                 "note": "Property Damage"
                              }
                           }
                        ]
                     }
                  },
                  {
                     "coverage": {
                        "Type": "Comprehensive",
                        "UniqueCoverageId": "ccsit:45930565",
                        "CoverageCode": "21000",
                        "Deductible": "100.00 usd",
                        "CovTermList": [{
                           "covTerm": {
                              "covTermPattern": "Comprehensive",
                              "coverageCategory": "Deductible",
                              "note": "Comprehensive"
                           }
                        }]
                     }
                  },
                  {
                     "coverage": {
                        "Type": "Collision",
                        "UniqueCoverageId": "ccsit:45930566",
                        "CoverageCode": "22000",
                        "Deductible": "500.00 usd",
                        "CovTermList": [{
                           "covTerm": {
                              "covTermPattern": "Collision",
                              "coverageCategory": "Deductible",
                              "note": "Collision"
                           }
                        }]
                     }
                  },
                  {
                     "coverage": {
                        "Type": "Loss of Use - K5",
                        "UniqueCoverageId": "ccsit:45930567",
                        "CoverageCode": "22011",
                        "Deductible": "0.00 usd",
                        "CovTermList": [
                           {
                              "covTerm": {
                                 "unitValue": "50.0000",
                                 "unitType": "money"
                              }
                           },
                           {
                              "covTerm": {
                                 "unitValue": "20.0000",
                                 "unitType": "days"
                              }
                           },
                           {
                              "covTerm": {
                                 "covTermPattern": "Collision Plus/Loss Of Use - K5",
                                 "coverageCategory": "Limit",
                                 "note": "K-5 Coverage: Maximum Amount"
                              }
                           }
                        ]
                     }
                  },
                  {
                     "coverage": {
                        "Type": "Towing & Road Service",
                        "UniqueCoverageId": "ccsit:45930568",
                        "CoverageCode": "40001",
                        "Deductible": "0.00 usd",
                        "CovTermList": [{
                           "covTerm": {
                              "covTermPattern": "Towing & Road Service",
                              "amount": "0.00",
                              "coverageCategory": "Limit",
                              "note": "Towing & Road Service"
                           }
                        }]
                     }
                  },
                  {
                     "coverage": {
                        "Type": "Medical Payments (Part 6)",
                        "UniqueCoverageId": "ccsit:45932585",
                        "CoverageCode": "30024"
                     }
                  },
                  {
                     "coverage": {
                        "Type": "Medical Coverage",
                        "UniqueCoverageId": "ccsit:45932586",
                        "CoverageCode": "30000"
                     }
                  }
               ],
               "Year": "2013",
               "Model": "JOURNEY 4D 2WD SXT",
               "PublicID": "ccsit:12496159"
            }
         }],
         "policyStatus": "In Force",
         "agentCode": "160676",
         "companyName": "Farmers Insurance Company of Arizona",
         "brandLogo": "Farmers",
         "policyNumber": "0190353081",
         "insuranceBrand": "Farmers Insurance",
         "InsuranceGroupIndicator": "FI",
         "policyStateCode": "NM",
         "policyState": "New Mexico",
         "claimGroup": "Farmers"
      },
      "claimsRepresentative": {
         "contact": {
            "FirstName": "FirstSuper",
            "LastName": "SuperUserOne",
            "DisplayName": "FirstSuper SuperUserOne",
            "PrimaryPhoneValue": "818-876-3853",
            "PrimaryPhoneType": "work"
         },
         "Supervisor": {
            "FirstName": "Alicia",
            "LastName": "Poslosky",
            "DisplayName": "Alicia Poslosky",
            "EmailAddress1": "inet.hpcs@gmail.com",
            "PrimaryPhoneType": "work",
            "PrimaryPhoneValue": "511-234-5678"
         },
         "zipWhipFlag": "false"
      },
      "ClaimNumber": "2050248304-1",
      "claimStatus": "Open",
      "SALN": "HY004575",
      "ClaimMilestone": "Wrapping Up",
      "IsExistPaperorDirectDepositeActivity": "false",
      "claimRowId": "ccsit:5736066",
      "isCATInspectionNeeded": "false",
      "isUploadReceipt": "false",
      "isWCWorkerInjured": "true",
      "LossDate": "2021-04-21",
      "ExposureList": [
         {
            "Exposure": {
               "rowId": "ccsit:9846730",
               "claimnt": {
                  "rowId": "ccsit:47553330",
                  "firstName": "ANTHONY",
                  "lastName": "AGOSTINELLI"
               },
               "ClaimUnitNumber": "2050248304-1-3",
               "ExposureStatus": "Open",
               "ExposureRepresentetive": {
                  "contact": {
                     "EmailAddress": "inet.hpcs@gmail.com",
                     "FirstName": "Dirk",
                     "LastName": "Ellerd",
                     "FullName": "Dirk Ellerd",
                     "FaxPhone": "5112345678",
                     "PrimaryPhone": "511-234-5678",
                     "WorkPhone": "5112345678"
                  },
                  "supervisor": {
                     "contact": {
                        "FullName": "Sherry Davis-Peace",
                        "FirstNAme": "Sherry",
                        "LastNAme": "Davis-Peace",
                        "MiddleName": "L",
                        "EmailAddress": "inet.hpcs@gmail.com"
                     }
                  }
               },
               "PropertyIncident": {},
               "ExposureCode": "PIPDamages",
               "ExposureName": "PIP",
               "DeductiblePaid": "0.00 usd"
            }
         },
         {
            "Exposure": {
               "rowId": "ccsit:9846731",
               "claimnt": {
                  "rowId": "ccsit:47553330",
                  "firstName": "ANTHONY",
                  "lastName": "AGOSTINELLI"
               },
               "ClaimUnitNumber": "2050248304-1-4",
               "ExposureStatus": "Open",
               "ExposureRepresentetive": {
                  "contact": {
                     "EmailAddress": "inet.hpcs@gmail.com",
                     "FirstName": "Dirk",
                     "LastName": "Ellerd",
                     "FullName": "Dirk Ellerd",
                     "FaxPhone": "5112345678",
                     "PrimaryPhone": "511-234-5678",
                     "WorkPhone": "5112345678"
                  },
                  "supervisor": {
                     "contact": {
                        "FullName": "Sherry Davis-Peace",
                        "FirstNAme": "Sherry",
                        "LastNAme": "Davis-Peace",
                        "MiddleName": "L",
                        "EmailAddress": "inet.hpcs@gmail.com"
                     }
                  }
               },
               "PropertyIncident": {},
               "ExposureCode": "MedPay",
               "ExposureName": "Med Pay",
               "DeductiblePaid": "0.00 usd"
            }
         },
         {
            "Exposure": {
               "rowId": "ccsit:9846416",
               "claimnt": {
                  "rowId": "ccsit:47553330",
                  "firstName": "ANTHONY",
                  "lastName": "AGOSTINELLI"
               },
               "ClaimUnitNumber": "2050248304-1-2",
               "ExposureStatus": "Open",
               "ExposureRepresentetive": {
                  "contact": {
                     "FirstName": "FirstSuper",
                     "LastName": "SuperUserOne",
                     "FullName": "FirstSuper SuperUserOne",
                     "PrimaryPhone": "818-876-3853",
                     "WorkPhone": "8188763853"
                  },
                  "supervisor": {
                     "contact": {
                        "FullName": "Alicia Poslosky",
                        "FirstNAme": "Alicia",
                        "LastNAme": "Poslosky",
                        "MiddleName": "R",
                        "EmailAddress": "inet.hpcs@gmail.com"
                     }
                  }
               },
               "PropertyIncident": { "PublicId": "ccsit:10400333" },
               "ExposureCode": "VehicleDamage",
               "ExposureName": "Vehicle",
               "DeductiblePaid": "0.00 usd",
               "ModifiedDeductible": "0.00 usd",
               "ModifiedDeductibleAmount": "0.00"
            }
         },
         {
            "Exposure": {
               "rowId": "ccsit:9846415",
               "claimnt": {
                  "rowId": "ccsit:47553330",
                  "firstName": "ANTHONY",
                  "lastName": "AGOSTINELLI"
               },
               "ClaimUnitNumber": "2050248304-1-1",
               "ExposureStatus": "Open",
               "ExposureRepresentetive": {
                  "contact": {
                     "FirstName": "FirstSuper",
                     "LastName": "SuperUserOne",
                     "FullName": "FirstSuper SuperUserOne",
                     "PrimaryPhone": "818-876-3853",
                     "WorkPhone": "8188763853"
                  },
                  "supervisor": {
                     "contact": {
                        "FullName": "Alicia Poslosky",
                        "FirstNAme": "Alicia",
                        "LastNAme": "Poslosky",
                        "MiddleName": "R",
                        "EmailAddress": "inet.hpcs@gmail.com"
                     }
                  }
               },
               "PropertyIncident": { "PublicId": "ccsit:10400333" },
               "ExposureCode": "VehicleDamage",
               "ExposureName": "Vehicle",
               "DeductiblePaid": "0.00 usd",
               "ModifiedDeductible": "500.00 usd",
               "ModifiedDeductibleAmount": "500.00"
            }
         }
      ],
      "LossCauseCode": "LostControlVehicle_Ext",
      "LossDescription": "test",
      "LossCauseName": "Lost control of vehicle - no better description applies",
      "ClaimTypeName": "Collision",
      "ClaimTypeCode": "collision",
      "LossLocation": {
         "AddressLine1": "319 DOLORES DR SW",
         "StateCode": "NM",
         "State": "New Mexico",
         "City": "ALBQRQE",
         "PostalCode": "87121",
         "Country": "US"
      },
      "contactList": [
         {
            "contact": {
               "EFTRecords": { "EFTRecord": {} },
               "relationshipToInsured": "",
               "EmailAddress": "farmersinet@gmail.com",
               "person": {
                  "AlternatePhone1": "5112345678",
                  "FirstName": "William",
                  "LastName": "Tobin",
                  "PrimaryPhoneValue": "511-234-5678"
               },
               "RelatedVehicleIncident": { "Location": {} },
               "VehicleOwner": {},
               "PrimaryPhoneType": "Work",
               "Addressline1": "6200 COORS BLVD NW STE K2",
               "City": "ALBUQUERQUE",
               "PostalCode": "87120-2701",
               "stateCode": "NM",
               "state": "New Mexico",
               "ClaimContactId": "ccsit:47553328",
               "PedestrianIndicator": "false",
               "DriverIndicator": "false",
               "PrimeRole": "Agent - Farmers",
               "InjuredIndicator": "false",
               "isCoveredParty": "false",
               "company": {}
            }
         },
         {
            "contact": {
               "EFTRecords": { "EFTRecord": {} },
               "relationshipToInsured": "",
               "person": {
                  "FirstName": "FRANCISCA",
                  "LastName": "AGOSTINELLI"
               },
               "RelatedVehicleIncident": { "Location": {} },
               "VehicleOwner": {},
               "Addressline1": "319 DOLORES DR SW",
               "City": "ALBQRQE",
               "PostalCode": "87121",
               "stateCode": "NM",
               "state": "New Mexico",
               "ClaimContactId": "ccsit:47553329",
               "PedestrianIndicator": "false",
               "DriverIndicator": "false",
               "PrimeRole": "Insured",
               "InjuredIndicator": "false",
               "isCoveredParty": "true",
               "company": {}
            }
         },
         {
            "contact": {
               "EFTRecords": { "EFTRecord": {} },
               "relationshipToInsured": "Self",
               "person": {
                  "FirstName": "ANTHONY",
                  "LastName": "AGOSTINELLI",
                  "HomePhone": "5053527439",
                  "PrimaryPhoneValue": "505-352-7439"
               },
               "RelatedVehicleIncident": { "Location": {} },
               "VehicleOwner": {},
               "Addressline1": "319 DOLORES DR SW",
               "City": "ALBQRQE",
               "PostalCode": "87121",
               "stateCode": "NM",
               "state": "New Mexico",
               "ClaimContactId": "ccsit:47553330",
               "EmailPreferred": "false",
               "EmailConfirmed": "false",
               "TextAlert": "false",
               "PedestrianIndicator": "false",
               "DriverIndicator": "true",
               "PrimeRole": "Insured",
               "InjuredIndicator": "true",
               "isCoveredParty": "true",
               "company": {}
            }
         }
      ],
      "CollDeductibleWaivedIndicator": "false",
      "ReportedDate": "2021-04-22",
      "SubrogationStatusCode": "NotReferred_Ext",
      "SubrogationStatus": "Not Referred",
      "DeductiblePaidAmount": "0.00",
      "IncidentList": [
         {
            "incident": {
               "IncidentRowId": "ccsit:10401092",
               "Subtype": "InjuryIncident",
               "property": {},
               "Vehicle": { "location": {} }
            }
         },
         {
            "incident": {
               "IncidentRowId": "ccsit:10400333",
               "Subtype": "VehicleIncident",
               "property": {
                  "isWaterServiceable": "false",
                  "isSmokeServiceable": "false",
                  "isBoardUpServiceable": "false",
                  "isGRPPropertyServiceable": "false"
               },
               "Vehicle": {
                  "Make": "DODGE TRUCK",
                  "Model": "JOURNEY 4D 2WD SXT",
                  "Year": "2013",
                  "VIN": "3C4PDCBG5DT670442",
                  "Style": "Suv_Ext",
                  "FarmersStyle": "Auto_Ext",
                  "PublicID": "ccsit:15702983",
                  "IsRepairlable": "false",
                  "DamagesInd": "true",
                  "VehicleLossParty": "insured",
                  "location": {},
                  "isPhotoAppSelected": "false",
                  "isGRPSelected": "false",
                  "CMPAllowService": "true",
                  "isInsVehicle": "true",
                  "Driver": "ccsit:47553330",
                  "InvolvedContactList": [{
                     "InvolvedContact": {
                        "RowId": "ccsit:47553330",
                        "FirstName": "ANTHONY",
                        "LastName": "AGOSTINELLI"
                     }
                  }]
               }
            }
         }
      ],
      "PaymentList": [
         {
            "Payment": {
               "costCategoryCode": "LossOfUse_Ext",
               "costCategoryDisplayName": "Loss of Use",
               "costTypeCode": "claimcost",
               "costTypeDisplayName": "Claim Cost",
               "check": {
                  "FirstPayeeRowId": "ccsit:47553330",
                  "IssueDate": "2020-09-22",
                  "PaidDate": "",
                  "PaymentMethodCode": "check",
                  "PaymentMethodName": "Check",
                  "ServicePdEnd": "",
                  "ServicePdStart": "",
                  "StatusCode": "issued",
                  "Status": "Issued",
                  "NetAmount": "153.00 usd",
                  "PrintLine2": "ANTHONY AGOSTINELLI",
                  "PrintLine3": "319 DOLORES DR SW",
                  "PrintLine4": "ALBQRQE, NM, 87121",
                  "CheckNumber": "1810063647",
                  "PayeeList": [{ "PayeeName": "ANTHONY AGOSTINELLI" }]
               }
            }
         },
         {
            "Payment": {
               "costCategoryCode": "LossOfUse_Ext",
               "costCategoryDisplayName": "Loss of Use",
               "costTypeCode": "claimcost",
               "costTypeDisplayName": "Claim Cost",
               "check": {
                  "FirstPayeeRowId": "ccsit:47553330",
                  "IssueDate": "2020-04-22",
                  "PaidDate": "",
                  "PaymentMethodCode": "check",
                  "PaymentMethodName": "Check",
                  "ServicePdEnd": "",
                  "ServicePdStart": "",
                  "StatusCode": "issued",
                  "Status": "Issued",
                  "NetAmount": "12.00 usd",
                  "PrintLine2": "ANTHONY AGOSTINELLI",
                  "PrintLine3": "319 DOLORES DR SW",
                  "PrintLine4": "ALBQRQE, NM, 87121",
                  "CheckNumber": "1810063648",
                  "PayeeList": [{ "PayeeName": "ANTHONY AGOSTINELLI" }]
               }
            }
         },
         {
            "Payment": {
               "costCategoryCode": "MaterialDamage_Ext",
               "costCategoryDisplayName": "Material Damage",
               "costTypeCode": "Expense_Ext",
               "costTypeDisplayName": "Expense",
               "check": {
                  "FirstPayeeRowId": "ccsit:47553330",
                  "IssueDate": "2020-05-22",
                  "PaidDate": "",
                  "PaymentMethodCode": "check",
                  "PaymentMethodName": "Check",
                  "ServicePdEnd": "",
                  "ServicePdStart": "",
                  "StatusCode": "issued",
                  "Status": "Issued",
                  "NetAmount": "153.00 usd",
                  "PrintLine2": "ANTHONY AGOSTINELLI",
                  "PrintLine3": "319 DOLORES DR SW",
                  "PrintLine4": "ALBQRQE, NM, 87121",
                  "CheckNumber": "1810063647",
                  "PayeeList": [{ "PayeeName": "ANTHONY AGOSTINELLI" }]
               }
            }
         },
         {
            "Payment": {
               "costCategoryCode": "MaterialDamage_Ext",
               "costCategoryDisplayName": "Material Damage",
               "costTypeCode": "Expense_Ext",
               "costTypeDisplayName": "Expense",
               "check": {
                  "FirstPayeeRowId": "ccsit:47553330",
                  "IssueDate": "2020-06-22",
                  "PaidDate": "",
                  "PaymentMethodCode": "check",
                  "PaymentMethodName": "Check",
                  "ServicePdEnd": "",
                  "ServicePdStart": "",
                  "StatusCode": "issued",
                  "Status": "Issued",
                  "NetAmount": "200.00 usd",
                  "PrintLine2": "ANTHONY AGOSTINELLI",
                  "PrintLine3": "319 DOLORES DR SW",
                  "PrintLine4": "ALBQRQE, NM, 87121",
                  "CheckNumber": "1810063647",
                  "PayeeList": [{ "PayeeName": "ANTHONY AGOSTINELLI" }]
               }
            }
         },
         {
            "Payment": {
               "costCategoryCode": "MaterialDamage_Ext",
               "costCategoryDisplayName": "Material Damage",
               "costTypeCode": "Expense_Ext",
               "costTypeDisplayName": "Expense",
               "check": {
                  "FirstPayeeRowId": "ccsit:47553330",
                  "IssueDate": "2020-07-22",
                  "PaidDate": "",
                  "PaymentMethodCode": "check",
                  "PaymentMethodName": "Check",
                  "ServicePdEnd": "",
                  "ServicePdStart": "",
                  "StatusCode": "issued",
                  "Status": "Issued",
                  "NetAmount": "300.00 usd",
                  "PrintLine2": "ANTHONY AGOSTINELLI",
                  "PrintLine3": "319 DOLORES DR SW",
                  "PrintLine4": "ALBQRQE, NM, 87121",
                  "CheckNumber": "1810063647",
                  "PayeeList": [{ "PayeeName": "ANTHONY AGOSTINELLI" }]
               }
            }
         },
         {
            "Payment": {
               "costCategoryCode": "MaterialDamage_Ext",
               "costCategoryDisplayName": "Material Damage",
               "costTypeCode": "Expense_Ext",
               "costTypeDisplayName": "Expense",
               "check": {
                  "FirstPayeeRowId": "ccsit:47553330",
                  "IssueDate": "2020-08-22",
                  "PaidDate": "",
                  "PaymentMethodCode": "check",
                  "PaymentMethodName": "Check",
                  "ServicePdEnd": "",
                  "ServicePdStart": "",
                  "StatusCode": "issued",
                  "Status": "Issued",
                  "NetAmount": "400.00 usd",
                  "PrintLine2": "ANTHONY AGOSTINELLI",
                  "PrintLine3": "319 DOLORES DR SW",
                  "PrintLine4": "ALBQRQE, NM, 87121",
                  "CheckNumber": "1810063647",
                  "PayeeList": [{ "PayeeName": "ANTHONY AGOSTINELLI" }]
               }
            }
         }
      ]
   }
}

module.exports = { long_claim, short_claim }