'use strict';

/**
 * Helper function to get data custom data to use in long claim api
 * @param {object} shortClaimData response recived from short claim api
 * @param {number} callerPhoneNum caller phone number enterd
 * @returns {object} claim data for matching claimcontact
 */
const getShortClaimDetails = (shortClaim, callerPhoneNum) => {
    let shortClaimData = {}
    let claim = shortClaim.searchCriteriaResponse.claims[0];
    let index = claim.claimContacts.findIndex(i => {
        if (i.telephones[0]["telephoneNumber"] !== undefined) {
            if (i.telephones[0].telephoneNumber === String(callerPhoneNum)) {
                return true
            }
        }
    })

    //get claim adjuster
    let adjuster = claim.claimAdjusters.find(i => i.COMIndicator === "Y")

    if (index !== -1) {
        let allTelephones = []

        //get zip code matching with contact details first name and last name
        let nameObj = claim.claimContacts[index]["personName"]
        let zipCodeIndex = claim.contact.findIndex(ci => ci.personName.firstName === nameObj.firstName && ci.personName.lastName === nameObj.lastName)
        if (zipCodeIndex !== -1) {
            shortClaimData['zipCode'] = claim.contact[zipCodeIndex].postalAddress.zipCode
        } else {
            shortClaimData['zipCode'] = false
        }

        //get all phonenumbers
        claim.claimContacts.forEach(i => {
            i.telephones.forEach(itemPhones => {
                if (itemPhones["telephoneNumber"] !== undefined) {
                    allTelephones.push(itemPhones)
                }
            })
        })

        let duplicateList = allTelephones.filter(i => i.telephoneNumber === String(callerPhoneNum))
        if (duplicateList.length > 1) {
            shortClaimData['isMultipleContact'] = true;
        } else {
            shortClaimData['isMultipleContact'] = false;
        }
        shortClaimData['isContactDetails'] = true;
        shortClaimData['roleId'] = claim.claimContacts[index]['roleId'];
        shortClaimData['claimAdjusters'] = adjuster ? adjuster : false;
        shortClaimData['dob'] = false
        return shortClaimData
    } else {
        shortClaimData['isMultipleContact'] = false;
        shortClaimData['isContactDetails'] = false;
        shortClaimData['roleId'] = false;
        shortClaimData['zipCode'] = false
        shortClaimData['claimAdjusters'] = adjuster ? adjuster : false;
        shortClaimData['dob'] = false
        return shortClaimData
    }
}


const getPaymentData = (longClaimData) => {
    let temp = [...longClaimData.Claims.PaymentList]
    if (temp.length === 0) {
        return null
    }
    else if (temp.length === 1) {
        return temp
    } else if (temp.length > 1) {
        temp.sort((x, y) => {
            return new Date(y.Payment.check.IssueDate) - new Date(x.Payment.check.IssueDate)
        })
        return temp.length > 5 ? temp.splice(0, 5) : temp
    } else {
        return null
    }

}

module.exports = {
    getShortClaimDetails,
    getPaymentData
}
