/* eslint-disable quotes */
/* eslint-disable semi */
/* eslint-disable indent */
/* eslint-disable no-unused-vars */

/**
 * Helper function to merge all text messages of fulfillment into single string
 * @param {Array} fulfillmentMessages Dialogflow fulfillment messages array
 * @returns {string}
 */
const getAllFulfillmentTextMessages = (fulfillmentMessages) => {
    let fullfillmentText = '';

    const messages = fulfillmentMessages.filter(eachMessage => {
        if (eachMessage.text && eachMessage.text.text[0]) {
            fullfillmentText = `${fullfillmentText} \n ${eachMessage.text.text[0]}`;
            return eachMessage.text.text[0];
        }
    });
    return fullfillmentText;
}
module.exports ={
  getAllFulfillmentTextMessages
}