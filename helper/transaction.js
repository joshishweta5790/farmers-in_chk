'use strict';

function get_response(number, numTransactions) {
    if (number == numTransactions - 1) {
        return "transaction_last"
    }
    else if (number == 0) {
        return "transaction_first"
    }
    else {
        return "transaction_middle"
    }
}

const getTransaction = async (transactionNumber, numTransactions, paymentDetails, responses) => {
    
    const transationParams = await paymentDetails[transactionNumber].Payment;
    const dfResponse = responses[get_response(transactionNumber, numTransactions)]

    // get variable values
    const dollars = transationParams.check.NetAmount;
    const name = transationParams.check.PrintLine2;
    const date = transationParams.check.IssueDate;
    const mode_of_transfer = transationParams.check.PaymentMethodName;
    const transferID = transationParams.check.CheckNumber;
    const address = transationParams.check.PrintLine3 + transationParams.check.PrintLine3;

    return (
        dfResponse
        .replace("%dollars", dollars)
        .replace("%name", name)
        .replace("%date", date)
        .replace("%mode_of_transfer", mode_of_transfer)
        .replace("%transfer_id", transferID)
        .replace("%address", address)
    );
}


module.exports = {
    getTransaction
}
