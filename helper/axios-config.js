'use strict'

const axios = require('axios');
const https = require('https');
const fs = require('fs');
const path = require('path');
const config = require('./../config')()

const _httpsAgent = new https.Agent({
    cert: fs.readFileSync(path.resolve(__dirname, "../pem-files/cert.pem"), 'utf8'),
    key: fs.readFileSync(path.resolve(__dirname, "../pem-files/privkey.pem"), 'utf8'),
})

function getAxiosInstance(tid) {
    axios.default.interceptors.request.use(req => {
        //set farmers root url
        req.baseURL = config.axiosBaseUrl;
        //set request header for all request
        req.headers = {
            'frms_appid': config.farmersReqData['frms_appid'],
            'frms_source': config.farmersReqData['frms_source'],
            'frms_tid': tid,
            'client_id': config.farmersReqData['client_id'],
            'client_secret': config.farmersReqData['client_secret'],
            'Content-Type': config.farmersReqData['Content-Type']
        }
        //set all certificates and keys
        req.httpsAgent = _httpsAgent;
        return req;
    })

    return axios.default
}

module.exports = getAxiosInstance