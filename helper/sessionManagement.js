"use strict";
//  document id is static for testing will make dynamic after getting converstion from  dfintent
const { Firestore } = require("@google-cloud/firestore");
const config = require("../config")();
const collection = config.firestore.collections["sessions"];
const db = new Firestore();
let date = new Date();
let dateString = date.getUTCFullYear() +"/"+ (date.getUTCMonth()+1) +"/"+ date.getUTCDate() + " " + date.getUTCHours() + ":" + date.getUTCMinutes() + ":" + date.getUTCSeconds();
     
const session_logs = {
    time:dateString,
    initial_message:'',
    adjuster_note:[],
    isNote: false
};


//..............Create a Session................//
const  createSession=async(user,agent,id,initialUtterance)=>{
    session_logs.adjuster_note=[];
    session_logs.initial_message =initialUtterance
    session_logs.adjuster_note.push(
        {    'user':user              
        }
    );
     session_logs.adjuster_note.push(
        {    'agent':agent              
        }
    );
    console.log("insdie create",session_logs)

    await db.collection(collection).doc(id).set(session_logs)
    .then((res)=>{
    }).catch((error)=>{
        console.log("error",error)
    });
};

//--------------update the session---------------------------//

const updateSession=async(type,agent,user,id,isNote)=>{  
    if(type=='both') {
        let userData ={
            "user": user
        }
        let agentData={
            "agent": agent
        }
        if(user){
        await  db.collection(collection).
        doc(id).
        update({
            isNote:isNote,
            adjuster_note:Firestore.FieldValue.arrayUnion(userData)
        }).then((res)=>{
            console.log("updated document ");
        
        }).catch((error)=>{
            console.log("inisde error",error);
        })

        }
        if(agent){
               await  db.collection(collection).
        doc(id).
        update({
            isNote:isNote,
            adjuster_note:Firestore.FieldValue.arrayUnion(agentData),
        }).then((res)=>{
            console.log("updated document ");
        
        }).catch((error)=>{
            console.log("inisde error",error);
        })
        }
      

    }
    else if (type=='agent') {
        let agentData={
            "agent": agent
        }
         writeData(agentData,id,isNote);
    }
    else {
        let userData ={
            "user": user
        }
     writeData(userData,id,isNote);

    }

}
const writeData = async(conv,id,isNote)=> {

        await  db.collection(collection).
        doc(id).
        update({
            isNote:isNote,
            adjuster_note:Firestore.FieldValue.arrayUnion(conv)
        }).then((res)=>{
            console.log("updated document ");
       
        }).catch((error)=>{
            console.log("inisde error",error);
        })



}


const deleteSession = async(id)=> {

   await db.collection(collection).doc(id).delete()
   .then((res)=>{
        console.log("deleted document ");
       
    }).catch((error)=>{
        console.log(error);
    })
}

const getSession = async(id)=>{
const snapshot = await db.collection(collection).doc(id).get();
const data = snapshot.data();
return data;
    
}



module.exports = {createSession,updateSession,deleteSession,getSession};