
const Session = require("../helper/sessionManagement")

const storeConversation =  async (fulfillment,sessionId, requestIntent)=> {
    let queryText = fulfillment._request.queryResult.queryText;
    const followupEventInput = fulfillment._response.followupEventInput||'';
    const initialConvIntent =[
        'document',
        'payment'
    ];
     const endConvIntent =[
        'success - no',
        'connectCR',
        'global_flow - transfer',
        'global_flow - fallback'    
    ];
    const noteIntent =[
        'global_flow - note'
    ];
    const eventsArray =['transaction']    
    let initialconv = await Session.getSession(sessionId);
    let isNote =false;
    let user =fulfillment._request.queryResult.queryText;
    let agent='';
    let initialNote ='';
    let type = 'both';
    if(initialconv){
        //if conv is already there 
        isNote = initialconv.isNote
        if (eventsArray.includes(queryText)) {
             type = 'agent';
         }
         if (followupEventInput) {
             type = 'user';
         }
         if (!eventsArray.includes(queryText) && !followupEventInput) {
              type = 'both';
         }
        if(fulfillment._response.fulfillmentText) {
                agent = fulfillment._response.fulfillmentText;
        }
        else {
                agent=fulfillment._request.queryResult.fulfillmentText;
        }
        if(noteIntent.includes(requestIntent)) {
              Session.updateSession(type,agent,user,sessionId,true);
        } 
        else {
            if(isNote) {
                Session.updateSession(type,agent,user,sessionId,isNote);
            } 
            else {
                Session.updateSession(type,agent,user,sessionId,isNote);
            }
        }   
    }
    else {
        //if conv is first time   document payment
        if(initialConvIntent.includes(requestIntent)) {
            initialNote = user
            if(fulfillment._response.fulfillmentText) {
                agent = fulfillment._response.fulfillmentText;
            }
            else {
                agent=fulfillment._request.queryResult.fulfillmentText;
            }
            if(type=='both'){
                Session.createSession(user,agent,sessionId,initialNote);
            }
        } 
        else {
            initialNote = user
            if(fulfillment._response.fulfillmentText) {
                agent = fulfillment._response.fulfillmentText;
            }
            else {
                agent=fulfillment._request.queryResult.fulfillmentText;
            }
            if(type=='both'){
                Session.createSession(user,agent,sessionId,initialNote);
            }

        }
    }


    if(endConvIntent.includes(requestIntent)) {
        //api call to send conversation
        //delete the conv
        console.log("end conversation")
    }
   
}

module.exports = {storeConversation};