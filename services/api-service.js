'use strict';

const _axios = require('./../helper/axios-config');
const { short_claim, long_claim } = require('./../mock-data/mulesoft-mock-data');
const config = require('./../config')()
const env = process.env.NODE_ENV;



/**
 * get short claim data from mulesoft
 * @param {number} claimNumber value from cisco
 */
const getShortClaim = async (claimNumber, sessionId) => {
  if (env === undefined || env === 'local') {
    return short_claim;
  } else {
    let reqBody = {
      "searchCriteria": {
        "ConsumerSystemName": config.farmersReqData['ConsumerSystemName'],
        "ClaimInfo": {
          "claimNumber": String(claimNumber),
          "claimSource": config.farmersReqData['claimSource']
        }
      }
    }

    return _axios(sessionId).post('/CCSIT/claimsms/v2/claimSearch?level=short', reqBody)
      .then(res => res.data)
      .catch(rej => {
        return Promise.reject(rej.response)
      })
  }
}


/**
 * get long claim data from mulesoft
 * @param {claimNumber} claimNumber Value from short Claim Inquiry with
 * @param {contactRowId} contactRowId value from short Claim Inquiry
 */
const getLongClaim = async (claimNumber, contactRowId, sessionId) => {

  if (env === undefined || env === 'local') {
    return long_claim;
  } else {
    let reqBody = {
      "retrieveFullClaimByClaimNumber": {
        "requestHeader": {
          "functionName": config.farmersReqData['functionName'],
          "systemName": config.farmersReqData['systemName'],
          //"messageReference": "123456789",
          "transactionDate": "2021-04-22T09:47:16.897Z"
        },
        "searchClaim": {
          "claimNumber": String(claimNumber),
          "contactRole": "",
          "contactRowId": String(contactRowId),
          "firstName": "",
          "lastName": ""
        }
      }
    }

    return _axios(sessionId).post(`/CCSIT/claimsms/v2/claims/${claimNumber}/claimdetails/cc`, reqBody)
      .then(res => res.data)
      .catch(rej => {
        return Promise.reject(rej.response)
      })
  }
}


/**
* Send message to customer
* @param {number} mobileNumber Customer Phone No.
* @param {number} partyid Pass MobileNumber in place of partyidin
* @param {string} textMessage Document Upload steps
*/
const sendMessage = async (mobileNumber, textMessage, partyid) => {
  if (env === undefined || env === 'dev' || env === 'local') {
    return Promise.resolve(textMessage);
  }
  return Promise.resolve("Success");
}


/**
* Update the response text
* @param {number} phoneNumber Phone No. of the customer called In.
* @param {number} adjusterNote full transcript of the call with the note to adjuster
* @param {string} text Initial Utterence
* @param {string} isNoteExists Pass true/false if customer leaves note
*/
const updateResponseText = async (phoneNumber, text, adjusterNote, isNoteExists, claimNumber, sessionId) => {

  if (env === undefined || env === 'dev' || env === 'local') {
    return Promise.resolve('Success');
  } else {
    let reqBody = {
      "textMessage": {
        "phoneNumber": "3104776187",
        "text": "Need to find status of my Payment",
        "adjusterNote": "I want to discuss regarding my payment. Can you gimme call back",
        "isNoteExists": "true"
      }
    }

    return _axios(sessionId).post(`CCSIT/clmntfms/v1/claims/${claimNumber}/textmessage`, reqBody)
      .then(res => res.data)
      .catch(rej => {
        return Promise.reject(rej.response)
      })
  }
}



module.exports = {
  getShortClaim,
  getLongClaim,
  sendMessage,
  updateResponseText
}